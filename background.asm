;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;This file displays the background of the debugging screen

;	FILL SCREEN WITH BACKGROUND COLOR
	and 	edi, 0		;0 edi
	mov	eax,COLOR_BACKGROUND
@@:
	stosw			;mov [es:edi],eax and inc edi
	cmp	edi,0x180000	;=1024x768x16bit
	jne	@b

;	SELECT COLOR
        mov     ax,COLOR_FOREGROUND
        mov     bx,ax                   ;are writing two at once, use same register
        shl     eax,16
        mov     ax,bx

;	DRAW BORDER -- Horizontal lines
	coordinates	ebx,SCREEN_LEFT+SCREEN_CORNERS,SCREEN_TOP
	coordinates	ecx,SCREEN_RIGHT-SCREEN_CORNERS,SCREEN_TOP

@@:
	mov	edi,ebx			;top line
	mov	[es:edi],eax		
	addCoordinates	edi,0,1		;duplicate under it
	mov	[es:edi],eax
	add	ebx,4
	cmp	ebx,ecx
	jna	@b

        coordinates     ebx,SCREEN_LEFT+SCREEN_CORNERS,SCREEN_BOTTOM
        coordinates     ecx,SCREEN_RIGHT-SCREEN_CORNERS,SCREEN_BOTTOM
@@:
        mov     edi,ebx                 ;bottom line
        mov     [es:edi],eax
        addCoordinates  edi,0,1         ;duplicate under it
        mov     [es:edi],eax
        add     ebx,4
        cmp     ebx,ecx
        jna     @b

;       DRAW BORDER -- Vertical lines
	xor		ecx,ecx
	coordinates	edi,SCREEN_LEFT,SCREEN_TOP+SCREEN_CORNERS
	coordinates	edx,SCREEN_RIGHT,SCREEN_TOP+SCREEN_CORNERS
	mov		ebx,SCREEN_BOTTOM-(SCREEN_CORNERS*2)-SCREEN_TOP
@@:
        mov     [es:edi],eax		;left one
	mov	[es:edx],eax		;right one
        addCoordinates	edi,0,1		;continue below
	addCoordinates	edx,0,1
	inc	ecx
        cmp	ecx,ebx 
        jna     @b

;       DRAW BORDER -- Diagonals
	xor		ecx,ecx
	coordinates     edi,SCREEN_LEFT,SCREEN_TOP+SCREEN_CORNERS
	coordinates	ebx,SCREEN_RIGHT-SCREEN_CORNERS,SCREEN_BOTTOM
@@:
	mov	[es:edi],eax		;upper left
	mov	[es:ebx],eax		;lower right
	addCoordinates	edi,1,0
	addCoordinates	ebx,1,0
	subCoordinates	edi,0,1
	subCoordinates	ebx,0,1
	inc	ecx
	cmp	ecx,SCREEN_CORNERS
	jna	@b

        xor		ecx,ecx
        coordinates     edi,SCREEN_RIGHT-SCREEN_CORNERS,SCREEN_TOP
	coordinates	ebx,SCREEN_LEFT,SCREEN_BOTTOM-SCREEN_CORNERS
@@:
        mov     [es:edi],eax		;upper right
	mov	[es:ebx],eax		;lower left
        addCoordinates  edi,1,1
	addCoordinates	ebx,1,1
        inc     ecx
        cmp     ecx,SCREEN_CORNERS
        jna     @b

;	BLACK CENTER
	mov	ax,COLOR_FOREGROUND	;look for foreground, background pair
	shl	eax,16
	mov	ax,COLOR_BACKGROUND
	xor	cl,cl
	coordinates	edi,0,SCREEN_TOP+2	;start under top line to avoid special condition
	coordinates	edx,0,SCREEN_BOTTOM	;stop above bottom line to avoid other special condition
@@:
	mov	ebx,[es:edi]
	add	edi,2
	cmp	cl,1b
	je	backgroundBlack		;if toggled on, write
backgroundBlackRet:
	cmp	edi,edx
	je	@f			;hop out when done
	cmp	eax,ebx
	jne	@b			;loop if not a state toggle
	xor	cl,1b
	jmp	@b			;loop
backgroundBlack:
	cmp	bx,COLOR_FOREGROUND
	je	backgroundBlackRet	;don't write if it's gold
	mov	[es:edi-2], word 0
        jmp     backgroundBlackRet
@@:
