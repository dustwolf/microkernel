;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Jure Sah, Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;	INITIALIZATION
	xor	ax,ax
	mov	ds,ax	;set data segment to 0
	mov	ah,0Fh
	int	10h	;get screen page

;	MAIN
	mov	si,welcomeMessage
	mov	ah,0Eh	;teletype output
@@:
	mov	al,[si]
	int	10h
	inc	si
	cmp	al,10
	jne	@b

;	jump over own data to the end
	jmp	@f

;       DATA
welcomeMessage  db      'Hello PiXiE world!',13,10

@@:
