# ---------------------------------------------------------------------------
#
#      Copyright 2008 Jure Sah
#
#      This file is part of microKernel.
#
#      microKernel is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      Please refer to the README file for additional information.
#
# ---------------------------------------------------------------------------

echo --------
echo INCLUDE TREE
echo --------
rm tree.tmp.sh 2> /dev/null
ls -1 *.asm | awk '{ print "echo ",$0,";cat ",$0," | grep include;echo --------" >> "tree.tmp.sh" }'
chmod +x tree.tmp.sh
./tree.tmp.sh
rm tree.tmp.sh
