;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;This too was Thomasz's code
;mostly replaced with Kohlrak's macro (some modifications by Dustwolf)
;Some credit can go to Dex, who's code was used to check this.

;Note: super duper 
;constants for later use with CS, DS, ES, FS, GS and SS registers
;first 2 bits: ring---------------------\
;next 1 bit: 0 if GDT, 1 if LDT--------\|
;next 13 bits: serial of entry--------\||
;				      ||\
;				    <=||||
;NULL_SELECTOR 		= 0000000000000000b
;CODE_SELECTOR 		= 1 shl 3
;DATA_SELECTOR 		= 2 shl 3
;VESA_SELECTOR 		= 3 shl 3

;Kohlrak's magic GDT macro
; access
gdt_Pr 		= 10000000b	;Present; must be 1, not needed in this code
gdt_Ex		= 00001000b	;Executable; 1 = code, 0 = data
gdt_DC		= 00000100b	;Direction Switch (for Data, growing, for Code, conforming [may not switch for Code, may enable ring changing... CHECK!!])
gdt_RW		= 00000010b	;1 = code R data RW, 0 = data R
gdt_Ac		= 00000001b	;Accessed; should never be used
; flags
gdt_Gr		= 10000000b	;Granularity; 0 = 1B, 1 = 4KiB
gdt_Sz		= 01000000b	;0 = 16 bit, 1 = 32 bit
; type
gdt_Loc		= 00000010b	;Local descriptor table (Not sure if this requires a seperate structure or not.)

;Usage:
; <label> GDTEntry <name><base>,<size>,<access>,<flags>,<Privl>
;
;  label is entry name
;  base is 32bit offset of start
;  size is 20bit size of block
;  access is access bits above, ORed together
;  flags is flags bits above, ORed together
;  Privl is 2bit ring
;
;Example:
; GDTring0 GDTEntry ring0, 0,100, gdt_Ex or gdt_RW, gdt_Gr or gdt_Sz, 0, 0
;
;Creates a 400 kb block starting from begining of memory, accessible to
;ring0, containing exectuable code, for 32bit mode. also, selector is 
;ring0_SELECTOR

macro GDTEntry name, base, size, access, flags, Privl, class {
name # _SELECTOR = ((($-gdt)/8) shl 3) or class or Privl
ptr # name = $
dw size and 0xFFFF
dd (base and 0xFFFFFF) or ((access or 10010000b or (Privl shl 5)) shl 24) ;That which is to the right of the first or is on the high order byte.
;dw (base and 0xFF shl 8) 
db flags or ((size and 0xF0000) shr 16)
db (base and 0xFF000000) shr 24 
}

gdt:
GDTEntry NULL, 0, 0, 0, 0, 0, 0
GDTEntry CODE, 0, 0xFFFFF, gdt_Ex, gdt_Gr or gdt_Sz, 0, 0
GDTEntry DATA, 0, 0xFFFFF, gdt_RW, gdt_Gr or gdt_Sz, 0, 0
GDTEntry VESA, 0,385, gdt_RW, gdt_Gr or gdt_Sz, 0, 0	;384,09.. * 4kb = 1024x768x16bit
GDTEntry STACK, 0x500, 1536, gdt_RW, gdt_Sz, 0, 0	;keep synced with stack32.asm
gdt_end:

GDTR:       dw gdt_end - gdt - 1
            dd gdt
