;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------
;
; This file contains the ISRs

ExceptionISR:				; exception handler
	jmp	hcf

nullISR:				; handler for all other interrupts
	iretd

macro Exception_ISR_Handler [number] {
Exception_ISR#number#:
	mov esi, number#h
	xor edi, edi
	call DBug_PrintNum
@@:
	hlt
	jmp $
}

macro Hardware_ISR_Handler [number] {
Exception_ISR#number#:
	pusha ;preserve if you want to jump back, or all heck breakes loose.
	mov esi, number#h
	xor edi, edi
	call DBug_PrintNum ;Output to show us that the interrupt works.
	popa
	iretd
}


	;We may want to eventually handle these individually, but so we know why we triple faulted, good just to handle them all this way for now.
	Exception_ISR_Handler 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 0A, 0B, 0C, 0D, 0E, 0F, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 1A, 1B, 1C, 1D, 1E, 1F

	Hardware_ISR_Handler 21, 22, 23, 24, 25, 26, 27, 28, 29, 2A, 2B, 2C, 2D, 2E, 2F

clockISR:
	iretd

keyboardISR:
	pusha

include "keyboard.asm"	;the keyboard ISR code

	popa
	iretd

mouseISR:
	pusha

include "mouse.asm"	;the mouse ISR code

	popa
	iretd

;keyboardISR:
;	push	eax
;	in	al,60h
;	cmp	al,1			; check for Esc key
;	je	rebootISR
;	mov	[0B8000h+2*(80+1)],al	; show the scan key
;	in	al,61h			; give finishing information
;	out	61h,al			; to keyboard...
;	mov	al,20h
;	out	20h,al			; ...and interrupt controller
;	pop	eax
;	iretd

rebootISR:
	mov	al,0FEh
	out	64h,al			; reboot computer
	jmp	rebootISR
