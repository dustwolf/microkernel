;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

; This file contains the macros for 32bit debugging
; copied the VESA one and made some changes
; see textResolution.asm for the basic idea

;Make sure you keep it in the boundries on the X and Y axis. No text wrapping, so you must do this *MANUALLY*.
jmp @f
USE32

;Little macros by DustWolf for positioning text on screen
;Since in text mode the coordinates are char positions anyway this is just dummy code
macro textCoordinates X,Y {
	coordinates	edi,X,Y
}
macro addTextCoordinates X,Y {
	addCoordinates	edi,X,Y
}

;uhhh... Kohlrak helllp.. wth does all this do?! XD
macro Prints X, Y, [string] {
	common
	textCoordinates X, Y
	forward
	mov esi, string
	call DBug_PrintString
}
macro addPrints X, Y, [string] {
	common
	addTextCoordinates X, Y
	forward
	mov esi, string
	call DBug_PrintString
}
macro PrintLines X, Y, [toPrint] {
	common
	local PrintLines_Data, ActualData, MoveDown
	MoveDown = Y ;Our counter
	jmp PrintLines_Data
ActualData:
	forward
db toPrint, 0
	common
PrintLines_Data:
	mov esi, ActualData
	forward
	textCoordinates X, MoveDown
	call DBug_PrintString
	MoveDown = MoveDown + 1 ;Not actually a real variable... This is an "assemble time variable."
}

macro halt32 {
@@: 
        hlt
        jmp     @b
}

USE16
@@:
