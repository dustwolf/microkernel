;; ---------------------------------------------------------------------------
;;
;;      Public Domain
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------
;
;http://board.flatassembler.net/topic.php?t=1139

macro bdmonths [dayscount] 
{ 
  forward 
   if bdDAY > dayscount 
    bdDAY = bdDAY-dayscount 
    bdMONTH = bdMONTH+1 
  forward 
   end if 
}  

bdTIME = %T 
bdDAY = bdTIME/(24*3600) 
bdDAY = bdDAY - (bdDAY+365)/(3*365+366) 
bdYEAR = 1970+bdDAY/365 
bdDAY = bdDAY mod 365 + 1 
bdMONTH = 1 
if bdYEAR mod 4 = 0 
   if bdYEAR mod 100 = 0
         if bdYEAR mod 400 = 0
                 bdFEBDAYS=29
          else
                        bdFEBDAYS=28 
         end if
      else
                bdFEBDAYS=29 
 end if
else 
     bdFEBDAYS=28 
end if 
bdmonths 31,bdFEBDAYS,31,30,31,30,31,31,30,31,30,31 

buildDate:
	db	'Build '
        db	((bdYEAR / 1000)+'0')
        db	(((bdYEAR mod 1000) / 100) + '0')
        db	(((bdYEAR mod 100) / 10) + '0')
        db	((bdYEAR mod 10) + '0')
        db	((bdMONTH / 10) + '0')
        db	((bdMONTH mod 10) + '0')
        db	((bdDAY / 10) + '0')
        db	((bdDAY mod 10) + '0') 
	db	0
