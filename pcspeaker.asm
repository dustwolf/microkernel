;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;PC speaker init and macros

;	MACROS
;macros run eax, should be saved beforehand
macro beepHz frequency {
	mov	al,0xB6
	out	0x43,al
	mov	ax,(1193180/frequency)
	out	0x42,al
	mov	al,ah		;move alah xD
	out	0x42,al
}

macro beepOn {
	in	al,0x61
	or	al,00000011b
	out	0x61,al
}

macro beepOff {
        in      al,0x61
        and	al,11111100b
        out     0x61,al
}

macro beepWait msec {
	mov	ecx,(msec/55)+1
@@:
	hlt
	loop	@b
}

macro beepFor msec {
beepOn
beepWait msec
beepOff
}

;	INITIALIZATION
beepHz 1700
beepFor 50
beepWait 50
beepFor 50
beepWait 50
beepFor 50
