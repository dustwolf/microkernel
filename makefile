# ---------------------------------------------------------------------------
#
#      Copyright 2008,2009 Jure Sah, Shane Tyler Yorks
#
#      This file is part of microKernel.
#
#      microKernel is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      Please refer to the README file for additional information.
#
# ---------------------------------------------------------------------------

#microkernel makefile
all:
	make pxe
	make floppy
	make cdrom

download:
	-mkdir dl
	fasm floppy.asm dl/microKernel_floppy.iso
	fasm main.asm dl/microKernel_pxe.bin
	fasm cdrom.asm dl/microKernel_cdrom.iso
	hg log -v --style=xml > dl/changelog.xml
	-rm dl/microKernel_src.zip
	zip -D -9 -l dl/microKernel_src.zip *
	chmod -R a+rX dl

pxe:
	mkdir -p tftpboot
	fasm main.asm tftpboot/kernel

cdrom:
	fasm cdrom.asm kernel.iso

floppy:
	fasm floppy.asm kernel.bin

run:
	make floppy
	bochs -q -f bochsrc &

debug:
	fasm textmodedebug.asm
	bochs -q -f debug.bochs

tree:
	chmod +x tree.sh
	./tree.sh
