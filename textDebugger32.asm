;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

; This file contains the functions for 32bit debugging

; not all of this has to be rewritten for text mode
; I just commented out some stuff for now so it assembles -- DustWolf

;Make sure you keep it in the boundries on the X and Y axis. No text wrapping, so you must do this *MANUALLY*.
jmp end_debugger

;=============================================================================================================
;ESI is the number to print.
DBug_PrintNum:
	mov bp, 8 ;8 times...
DBug_PrintNumLoop:
	xor ebx, ebx
	shld ebx, esi, 4
	or bl,	'0'
	cmp 	bl,'9'	;hex top
	jng	@f
	add	bl,7	;offset to A
@@:	
	shl esi, 4
	push ebp
	call DBug_PrintLetter
	pop ebp
	dec bp
	jnz DBug_PrintNumLoop
	mov 	bl,'h'
	call DBug_PrintLetter
	ret
;=============================================================================================================
;Note: After this, all general registers except ESP are TRASHED.
DBug_PrintString:
	lods BYTE [ds:esi]
	or al, al
	jnz @f
	ret
@@:
	xor ebx, ebx ;This'll need to be clear.
	mov bl, al
	call DBug_PrintLetter
	jmp DBug_PrintString
;=============================================================================================================
;It assumes that ebx contains the letter (bl, actually, and the rest is 0ed). It also assumes you're printing an available letter.

; this code is kinda hacky... 0xB8000 will have to be the segment later

DBug_PrintLetter:
	mov	[0xB8000+edi+1], byte 0x0F 	;white
	mov	[0xB8000+edi],bl
	addTextCoordinates	1,0
ret
;=============================================================================================================
end_debugger:
