;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah, Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;This file sets up text mode, 32bit code

;include	"colorscheme.asm"	;do we need another one of these?

;	SELECT TEXT MODE GRAPHICS SEGMENT	not needed yet
;	mov 	ax, TEXTMODE_SELECTOR
;	mov 	es, ax

; some CLS style code here
	textCoordinates		0,0
	mov	bl,' '
	mov	ecx,TEXT_WIDTH*TEXT_HEIGHT
@@:
	call	DBug_PrintLetter
	loop	@b
