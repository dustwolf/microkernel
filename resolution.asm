;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

VESA_MODE 	equ 	0x116	;116h 1024x768x32K = 15bit
VESA_WIDTH      equ	1024d
VESA_HEIGHT	equ	768d
SCREEN_CORNERS	equ	40d	;this is the size of the fancy corners
SCREEN_OFFSET	equ	15d
SCREEN_TOP	equ	SCREEN_OFFSET
SCREEN_BOTTOM	equ	(VESA_HEIGHT-SCREEN_OFFSET)
SCREEN_LEFT	equ	SCREEN_OFFSET
SCREEN_RIGHT	equ	(VESA_WIDTH-SCREEN_OFFSET)

;this is here for now probably best kept here since this may change if
;somebody picks a weird resolution

macro coordinates register,X,Y {
	mov	register,((X)+(Y)*VESA_WIDTH)*2
}

macro addCoordinates register,X,Y {
	add	register,((X)+(Y)*VESA_WIDTH)*2
}

macro subCoordinates register,X,Y {
        sub     register,((X)+(Y)*VESA_WIDTH)*2
}
