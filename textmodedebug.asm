;; ---------------------------------------------------------------------------
;;
;;	Copyright 2008,2009 Jure Sah, Shane Tyler Yorks
;;
;;	This file is part of microKernel.
;;
;;	microKernel is free software: you can redistribute it and/or modify
;;	it under the terms of the GNU General Public License as published by
;;	the Free Software Foundation, either version 3 of the License, or
;;	(at your option) any later version.
;;
;;	Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

format binary as "iso"

include "debugger16Macros.asm"  ;contains macros for 16bit debugging
include "eltorito.asm"		;makes the kernel loadable from cdrom
	cli			;DISABLE INTERRUPTS
include "textResolution.asm"	;code for text mode screen size and such
include "debugger32textMacros.asm"	;code for text mode debug out
include "transition32.asm"	;goes into 32bit protected mode
                                ;and loads the GDT (GDT.asm)
include "stack32.asm"		;32bit stack
	sti
include "textDebugger32.asm"	;this one will work better
include "textmode.asm"		;init text mode
include "fancy32.asm"		;the fancy stuff

include "end.asm"		;does a loop of eternal nothingness
