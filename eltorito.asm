;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah, Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;"All numbers in this document are hexadecimal unless otherwise specified." ~ El Torito Specification

;ElTorito spec:
;
;1 cdrom sector = 0x800 bytes = 2048 bytes (always)
;
;Primary Volume Descriptor...Volume Descriptor Set Terminator @ sector16 (iso9660)
;Boot Record @ sector17 (eltorito, fig.7)
; points to Boot Catalog 
;  points to image			I wonder what I meant by the below two items... will have to reread that document tommorow
;   is a validation entry (fig.2)
;   is kernel (fig.3)

macro BE number, type { ;Big Endian
Local typeloop, newnumber, tempnumber
newnumber = 0 ;initialization
tempnumber = number ;to prevent possible but unlikely destruction of variables
if `type = "dw"
	typeloop = 2
else
	typeloop = 4
end if
while typeloop
	newnumber = (newnumber shl 8) or (tempnumber and 0xFF)
	tempnumber = tempnumber shr 8
	typeloop = typeloop - 1
end while
type newnumber
}

macro DE number, type { ;Double Endian
type number ;Little endian
BE number, type
}
;=============================================================================================================================================================================
rb 8000h ;Blank space... Maybe usable?
;=============================================================================================================================================================================
;       PRIMARY VOLUME DESCRIPTOR (key = unused, must be null and stuff)
        		db      1       ;it's a primary volume descriptor
        		db      'CD001'
        		db      1       ;v1 - international standard
        		db      0       ;Zeroed flags... Keep them that way...
times 32	 	db 	0      ;Might've been intended for specialized purpose, but they don't seem to be implemented anywhere.
times 32		db 	0
        		rb      8       ;key
			DE	(filesz)/800h, dd ;8.4.8 Volume Space Size (BP 81 to 88) NOTE: Keep file aligned to 2048, or this line breaks...
        		rb      32      ;key
;Figure out these 1s...
        		DE      1, dw     ;8.4.10 Volume Set Size (BP 121 to 124)
        		DE      1, dw     ;8.4.11 Volume Sequence Number (BP 125 to 128)
        		DE      800h, dw     ;8.4.12 Logical Block Size (BP 129 to 132)
        		DE      0x0, dd     ;8.4.13 Path Table Size (BP 133 to 140)
        		dd      0     ;8.4.14 Location of Occurrence of Type L Path Table (BP 141 to 144) ;Segment of path table...
        		dd      0     ;8.4.15 Location of Optional Occurrence of Type L Path Table (BP 145 to 148) 
        		BE      0, dd     ;8.4.16 Location of Occurrence of Type M Path Table (BP 149 to 152) ;Politically correct?
        		BE      0, dd     ;8.4.17 Location of Optional Occurrence of Type M Path Table (BP 153 to 156)
			db	0x22
			rb 	0x21 ;We can null this whole thing! =)      		
times 128		db	0     ;8.4.19 Volume Set Identifier (BP 191 to 318)
times 128		db      0     ;8.4.20 Publisher Identifier (BP 319 to 446) this needs a FS and is optional
times 128		db      0     ;8.4.21 Data Preparer Identifier (BP 447 to 574) this needs a FS and is optional
times 128		db      0     ;8.4.22 Application Identifier (BP 575 to 702) this needs a FS and is optional  
times 37		db      0      ;8.4.23 Copyright File Identifier (BP 703 to 739) this needs a FS and is seemingly not optional?
times 37		db      0      ;8.4.24 Abstract File Identifier (BP 740 to 776) this needs a FS and is seemingly not optional?
times 37		db      0      ;8.4.25 Bibliographic File Identifier (BP 777 to 813) this needs a FS and is seemingly not optional? 
        		rb      17      ;8.4.26 Volume Creation Date and Time (BP 814 to 830) YES 17!!!
       			rb      17      ;8.4.27 Volume Modification Date and Time (BP 831 to 847)
        		rb      17      ;8.4.28 Volume Expiration Date and Time (BP 848 to 864)
        		rb      17      ;8.4.29 Volume Effective Date and Time (BP 865 to 881)
        		db      0       ;v1 - international standard which was ment to be broken (key)
        		db      0       ;key
        		rb      512     ;8.4.32 Application Use (BP 884 to 1 395) this can stay null NOTE: Sometimes " "ed.
        		rb      653     ;Might not be key...
;=============================================================================================================================================================================
;       BOOT RECORD VOLUME DESCRIPTOR
cdromBootRecord:
        db      0       ;key
        db      'CD001'
        db      1       ;v1 - international standard
        db      'EL TORITO SPECIFICATION'       ;23byte + padding = 32byte
        rb      9                               ;this is the padding for above
        rb      32      ;key
        dd      cdromBootCatalog/800h
        rb      1973    ;key
;=============================================================================================================================================================================
;       VOLUME DESCRIPTOR SET TERMINATOR
        db      255     ;it's a set terminator
        db      'CD001'
        db      1       ;v1 - international strandard
        rb      2041    ;padding till end of sector
;=============================================================================================================================================================================
;       BOOT CATALOG

;El Torito specification says (about the boot catalog):
; First entry must be the Validaton Entry (fig. 2)
; Followed by the Initial/Default Entry (fig. 3)

cdromBootCatalog:
;	VALIDATION ENTRY
	db	1	;Header ID, must be 01
	db	0	;Platform ID, 0 = 80x86
	dw	0	;must be 0
	rb	24	;ID string. This is intended to identify the manufacturer/developer of the CD-ROM. FILL ME IN
	dw	0x55AA	;Checksum Word. This sum of all the words in this record should be 0. (Is this just a mear co-incidence?)
	db	0x55	
	db	0xAA	;sound familiar?

;	INITAL/DEFAULT ENTRY
	db	0x88	;0x88 = bootable
	db	0	;0 = No emulation (lucky us as beyond 0 it gets bitmasky)
	dw	0	;Load segment. 0 = 0x7C0
	db	0	;System Type. This must be a copy of byte 5 (System Type) from the Partition Table found in the boot image. ?!?!? FILL ME IN (Works, but...)
	db	0	;must be 0
	dw	(filesz/800h)	;Sector Count. 
	dd	cdromEntry/800h	;Load RBA. This is the start address of the virtual disk. CD’s use Relative/Logical block addressing. This is proabaly cdromEntry FILL ME IN
	rb	20	;must be 0
times 2048-($-cdromBootCatalog) db 0
;=============================================================================================================================================================================
cdromEntry:
org 0x7c00
microkernelStart:

	xor	ax,ax
	mov	ds,ax
	mov	es,ax
	mov	fs,ax
	mov	gs,ax
	mov	ss,ax
