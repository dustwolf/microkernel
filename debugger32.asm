;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

; This file contains the functions for 32bit debugging

;Make sure you keep it in the boundries on the X and Y axis. No text wrapping, so you must do this *MANUALLY*.
jmp end_debugger

include "DebugBitmapFont.inc"
;======================================================================================================================================
;ESI is the number to print.
DBug_PrintNum:
	mov bp, 8 ;8 times...
@@:
	xor ebx, ebx
	shld ebx, esi, 4
	or bl, 0x30
	shl esi, 4
	push ebp
	call DBug_PrintLetter
	pop ebp
	dec bp
	jnz @b
	or bl, 80h ;our special h
	call DBug_PrintLetter
	ret
;======================================================================================================================================
;Note: After this, all general registers except ESP are TRASHED.
DBug_PrintString:
	lods BYTE [ds:esi]
	or al, al
	jnz @f
	ret
@@:
	xor ebx, ebx ;This'll need to be clear.
	mov bl, al
	call DBug_PrintLetter
	jmp DBug_PrintString
;======================================================================================================================================
;It assumes that ebx contains the letter (bl, actually, and the rest is 0ed). It also assumes you're printing an available letter.
DBug_PrintLetter:
	mov bp, COLOR_TEXT
	mov ecx, 64 ;This is 8x8, the dimensions of our letters. We'll use this later.

	bt bx, 7 ;our special h
	jnc @f
	mov ebx, SPECIALH
	jmp DBug_PrintLetter_ActuallyPrintLetter
@@:
	bt bx, 6
	jc DBug_PrintLetter_GetLetter
	bt bx, 5
	jc DBug_PrintLetter_GetNumPunc
	bt bx, 4
	sub edi, 16
	ret
DBug_PrintLetter_GetNumPunc:
	bt bx, 4 ;If not space, carry is set.
	jc @f
	add edi, 16
	ret
@@:	and bl, 0xF
	shl bl, 3 ;This should be it...
	add ebx, DBNUMBERS
	jmp DBug_PrintLetter_ActuallyPrintLetter
DBug_PrintLetter_GetLetter:
	and bl, 0x1F
	dec bl ;A's not quite right on the line...
	shl bl, 3
	add ebx, DBLETTERS
DBug_PrintLetter_ActuallyPrintLetter:
	mov edx, DWORD [ebx]

@@:
	xor ax, ax ;clear
	bt edx, ecx ;Yulp, ecx'll automod to 31 and less. I doubt it internally uses div, though.
	cmovc ax, bp
	stosw

	dec cl
	jz DBug_PrintLetterDone

	test cl, 111b ;mod 2^111b(7d)+1 (This'll help check for intervals of 8)
	jnz @b
	add edi, VESA_WIDTH*2-16 ;Re-add the 8 spaces and move down a line.

	test cl, 11000b ;Check if it's time to refill.
	jnz @b
	mov edx, DWORD [ebx+4]
	jmp @b

DBug_PrintLetterDone:
	sub edi, VESA_WIDTH*2*7 ;Get us back where we belong.
ret
;======================================================================================================================================
end_debugger:
