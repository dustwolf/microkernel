;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Jure Sah, Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;This file contains the colors to be used in VESA graphics in 15bit

;				FEDCBA9876543210
;				TRRRRRGGGGGBBBBB
COLOR_BACKGROUND	equ	0011000000000000b	;dark red
COLOR_FOREGROUND	equ	0111111010100000b	;gold
COLOR_SPECIAL		equ	0111111111111111b	;white
COLOR_BODY		equ	1000000000000000b	;black
COLOR_TEXT		equ	COLOR_FOREGROUND	;gold
