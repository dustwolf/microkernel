;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------
;
; This file contains the IDT itself

int_seg_present = 10000000b
int_32bit = 1000b
int_tskGate = 1 ;Note: DO *NOT* use int_32bit with int_tskGate.
int_intGate = 2
int_trpGate = 3
struc IntServRout offset, selector, flags, privl { ;made structure for easy readability in the table...
	dw offset and 0xFFFF
	dw selector 		;Segment or TSS selector
	db 0 			;Must remain 0....
	db flags or (privl shl 6) or 0100b ;
	dw offset shr 16
}

;USAGE: name (unused) IntServRout ProcedureAddress, SegmentSelector or TSS Selector, Flags, Privledge Level
;tskGate (Task Gate): Everything gets preserved... Basically a task switch, so offset gets ignored...
;intGate (Interrupt Gate): Auto CLI and STI.
;trpGate (Trap Gate): Can be interrupted by a hardware interrupt...

IDTR:					; Interrupt Descriptor Table Register
  dw IDT_END-IDT				; limit of IDT (size minus one)
  dd IDT					; linear address of IDT

;exception_gate:
;  dw exceptionISR and 0FFFFh,CODE_SELECTOR
;  dw 8E00h,exceptionISR shr 16
;  ;dd 0, 0 ;SUPER NULL ISR
;interrupt_gate:
;  dw nullISR and 0FFFFh,CODE_SELECTOR
;  dw 8F00h,nullISR shr 16
;  dd 0, 0 ;SUPER NULL ISR

IDT:
int00h IntServRout Exception_ISR00, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int01h IntServRout Exception_ISR01, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int02h IntServRout Exception_ISR02, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int03h IntServRout Exception_ISR03, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int04h IntServRout Exception_ISR04, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int05h IntServRout Exception_ISR05, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int06h IntServRout Exception_ISR06, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int07h IntServRout Exception_ISR07, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int08h IntServRout Exception_ISR08, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int09h IntServRout Exception_ISR09, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int0Ah IntServRout Exception_ISR0A, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int0Bh IntServRout Exception_ISR0B, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int0Ch IntServRout Exception_ISR0C, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int0Dh IntServRout Exception_ISR0D, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int0Eh IntServRout Exception_ISR0E, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int0Fh IntServRout Exception_ISR0F, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int10h IntServRout Exception_ISR10, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int11h IntServRout Exception_ISR11, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int12h IntServRout Exception_ISR12, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int13h IntServRout Exception_ISR13, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int14h IntServRout Exception_ISR14, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int15h IntServRout Exception_ISR15, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int16h IntServRout Exception_ISR16, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int17h IntServRout Exception_ISR17, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int18h IntServRout Exception_ISR18, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int19h IntServRout Exception_ISR19, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int1Ah IntServRout Exception_ISR1A, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int1Bh IntServRout Exception_ISR1B, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int1Ch IntServRout Exception_ISR1C, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int1Dh IntServRout Exception_ISR1D, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int1Eh IntServRout Exception_ISR1E, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int1Fh IntServRout Exception_ISR1F, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0
int20h IntServRout clockISR, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0		;IRQ 0
int21h IntServRout keyboardISR, CODE_SELECTOR, int_seg_present or int_32bit or int_intGate, 0		;IRQ 1
int22h IntServRout Exception_ISR22, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 2
int23h IntServRout Exception_ISR23, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 3
int24h IntServRout Exception_ISR24, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 4
int25h IntServRout Exception_ISR25, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 5
int26h IntServRout Exception_ISR26, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 6
int27h IntServRout Exception_ISR27, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 7
int28h IntServRout Exception_ISR28, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 8
int29h IntServRout Exception_ISR29, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 9
int2Ah IntServRout Exception_ISR2A, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 10
int2Bh IntServRout Exception_ISR2B, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 11
int2Ch IntServRout mouseISR, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0		;IRQ 12
int2Dh IntServRout Exception_ISR2D, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 13
int2Eh IntServRout Exception_ISR2E, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 14
int2Fh IntServRout Exception_ISR2F, CODE_SELECTOR, int_seg_present or int_32bit or int_trpGate, 0	;IRQ 15
IDT_END:
