;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah, Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;this is to contain the code that does the VESA graphics mode flip
;uses int 10h, al = 4Fh
;
; this is 16bit code
;
;http://www.vesa.org/public/VBE/vbe3.pdf

        ;mov     ax,cs           ;set ES to CS
        ;mov     es,ax

include "resolution.asm"	;this contains the resolution equs

	;fill in the current info first -- dex
	mov	ax,0x4F01	;VESA SuperVGA BIOS - GET SuperVGA MODE INFORMATION
	mov	cx,0x4000 or VESA_MODE 	;0x4000 = enable LFB
        mov     di,VESAInfo	;table of VESA info 
	mov	bx,cx		;actually needed for the next call
	int	10h

	;set mode, use stuff set before
	mov     ax,0x4F02       ;VESA SuperVGA BIOS - SET SuperVGA VIDEO MODE
	int	10h
	cmp	al,0x4F
	jne	VESAunsupported
	or	ah,ah
	jnz	VESAfailed

	;check current info
	mov	ax,0x4F01	;VESA SuperVGA BIOS - GET SuperVGA MODE INFORMATION
	mov	cx,0x4105
	mov	di,VESAInfo
	int	10h
	or	ah,ah		;if this didn't work, go caboom
	jnz	hcf

	;Last call to load the GDT registers!
	mov eax, [vLFB]
	mov [ptrVESA+7], al
	shr eax, 8
	or [ptrVESA+5], eax

	jmp	VESAend

VESAInfo:
	dw	?	;mode attributes (see #00080)
	db	?	;window attributes, window A (see #00081)
	db	?	;window attributes, window B (see #00081)
	dw	?	;window granularity in KB
	dw	?	;window size in KB
vA	dw	?	;start segment of window A (0000h if not supported)
vB	dw	?	;start segment of window B (0000h if not supported)
	dd	?	;-> FAR window positioning function (equivalent to AX=4F05h)
	dw	?	;bytes per scan line
;---remainder is optional for VESA modes in v1.0/1.1, needed for OEM modes---
	dw	?	;width in pixels (graphics) or characters (text)
	dw	?	;height in pixels (graphics) or characters (text)
	db	?	;width of character cell in pixels
	db	?	;height of character cell in pixels
	db	?	;number of memory planes
	db	?	;number of bits per pixel
	db	?	;number of banks
	db	?	;memory model type (see #00082)
	db	?	;size of bank in KB
	db	?	;number of image pages (less one) that will fit in video RAM
	db	?	;reserved (00h for VBE 1.0-2.0, 01h for VBE 3.0)
;---VBE v1.2+ ---
	db	?	;red mask size
	db	?	;red field position
	db	?	;green mask size
	db	?	;green field size
	db	?	;blue mask size
	db	?	;blue field size
	db	?	;reserved mask size
	db	?	;reserved mask position
	db	?	;direct color mode info
;VBE 2.0
vLFB	dd	0	;PhysBasePtr
	dd	?	;OffScreenMemOffset
	dw	?	;OffScreenMemSize
;VBE 2.1
	dw	?	;LinbytesPerScanLine
	db	?	;BankNumberOfImagePages
	db	?	;LinNumberOfImagePages
	db	?	;LinRedMaskSize
	db	?	;LinRedFieldPosition
	db	?	;LingreenMaskSize
	db	?	;LinGreenFieldPosition
	db	?	;LinBlueMaskSize
	db	?	;LinBlueFieldPosition
	db	?	;LinRsvdMaskSize
	db	?	;LinRsvdFieldPosition
	rb	194	;res2[194]
	db	'$'


VESAfailedMsg		db	'VESA graphics mode flip failed',13,10
VESAunsupportedMsg	db	'VESA graphics mode flip unsupported',13,10

VESAfailed:
	debug16GetPage
	debug16Type VESAfailedMsg,10
	halt16

VESAunsupported:
	debug16GetPage
	debug16Type VESAunsupportedMsg,10
	halt16

VESAend:
