;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah, Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;This file displays the fancy stuff, 32bit code

; I split this code into graphics.asm and fancy32.asm
; This code should be portable between VESA and text mode
;			-- DustWolf

	Prints	0,1,buildDate
	Prints	0,0,lMicroKernel
	
	textCoordinates	0,3		;Written by
        call    DBug_PrintString

	textCoordinates 0,4		;DustWolf Kohlrak
	call	DBug_PrintString

	textCoordinates	0,6
	mov	esi,lSize
	call	DBug_PrintString

	mov	esi,microkernelEnd-microkernelStart
	call	DBug_PrintNum

	textCoordinates	0,8
	mov	esi,lKb
	call	DBug_PrintString

        textCoordinates 0,11
        mov     esi,lMouse
        call    DBug_PrintString

	jmp	@f
lMicroKernel	db	'microKernel',0
		db      'Written by',0
		db      'DustWolf and Kohlrak',0
lSize		db	'Kernel size ',0
lKb		db	'Keyboard input',0
lMouse		db	'Mouse input',0

include	"builddate.asm"			;contains a fancy for displaying the build date

@@:
