@echo off
REM ---------------------------------------------------------------------------
REM
REM      Copyright 2008 Jure Sah
REM
REM      This file is part of microKernel.
REM
REM      microKernel is free software: you can redistribute it and/or modify
REM      it under the terms of the GNU General Public License as published by
REM      the Free Software Foundation, either version 3 of the License, or
REM      (at your option) any later version.
REM
REM      Please refer to the README file for additional information.
REM
REM ---------------------------------------------------------------------------
REM
REM microkernel makefile for Windows

IF /i "%1" == "" (goto :all)
IF /i "%1" == "all" (goto :all) 
IF /i "%1" == "download" (goto :download) 
IF /i "%1" == "pxe" (goto :pxe) 
IF /i "%1" == "floppy" (goto :floppy) 
echo Not implemented
goto :end

:all
set /a flag=1
goto :pxe

:download
mkdir dl
fasm floppy.asm dl\microKernel_floppy.iso
fasm main.asm dl\microKernel_pxe.bin
goto :end

:pxe
fasm main.asm tftpboot\kernel
if %flag%==1 (goto :floppy)
goto :end

:floppy
fasm floppy.asm kernel.bin
goto :end

:end
