;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Jure Sah, Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------
;
;this code activates A20

macro A20Fast {
	in	al, 0x92
	or	al, 2
	out	0x92,al
}

;All output to port 0x60 or 0x64 must be preceded by waiting for bit number 1 (value=2)
;of port 0x64 to clear. Similarly, bytes cannot be read from port 0x60 until bit number 0
;(value=1) of port 0x64 is set. See PS2 Keyboard for further details.
;http://wiki.osdev.org/Mouse_Input#Waiting_to_Send_Bytes_to_Port_60_and_64

macro Port64wait {
;	WAIT UNTIL BIT 1 = 0
@@:
	in	al,0x64
	test	al,00000010b
	jnz	@b	  
}

macro Port64out x {
	Port64wait
	mov	al,x
	out	0x64,al
}

macro Port60out x {
        Port64wait
        mov     al,x
        out     0x60,al
}

macro Port60in {
;       WAIT UNTIL BIT 0 = 1
@@:
        in      al,0x64
        test    al,00000001b
        jz      @b

        in	al,0x60
}

macro A20Slow {
	Port64out	0xD1	
	Port60out	0xCF	;0xDF CF doesn't disable mouse and leaves IRQs unset
}

;	DO IT	
	A20Slow
