;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------
;
; This file contains macros required for 16bit debugging

macro debug16GetPage {
        mov     ah,0Fh
        int     10h     ;get screen page
}

;debug16Type messageoffset,end_char
macro debug16Type message, end {
        mov     si,message
        mov     ah,0Eh  ;teletype output
@@:
        mov     al,[si]
        int     10h
        inc     si
        cmp     al,end
        jne     @b
}

macro halt16 {
@@:
	hlt
	jmp     @b
}
