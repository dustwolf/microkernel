;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

; This file contains the macros for 32bit debugging

;Make sure you keep it in the boundries on the X and Y axis. No text wrapping, so you must do this *MANUALLY*.
jmp @f
USE32

include "colorscheme.asm"       ;this contains the color constants to be used here

;Little macros by DustWolf for positioning text on screen
macro textCoordinates X,Y {
        mov     edi,((SCREEN_LEFT+SCREEN_CORNERS+(X)*8)+(SCREEN_TOP+SCREEN_CORNERS+(Y)*8)*VESA_WIDTH)*2
}
macro addTextCoordinates X,Y {
        add	edi,((X)*8+(Y)*8*VESA_WIDTH)*2
}

macro Prints X, Y, [string] {
	common
	textCoordinates X, Y
	forward
	mov esi, string
	call DBug_PrintString
}

macro addPrints X, Y, [string] {
	common
	addTextCoordinates X, Y
	forward
	mov esi, string
	call DBug_PrintString
}
macro PrintLines X, Y, [toPrint] {
	common
	local PrintLines_Data, ActualData, MoveDown
	MoveDown = Y ;Our counter
	jmp PrintLines_Data
ActualData:
	forward
db toPrint, 0
	common
PrintLines_Data:
	mov esi, ActualData
	forward
	textCoordinates X, MoveDown
	call DBug_PrintString
	MoveDown = MoveDown + 1 ;Not actually a real variable... This is an "assemble time variable."
}

macro halt32 {
@@: 
        hlt
        jmp     @b
}

USE16
@@:
