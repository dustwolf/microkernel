;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

cdromEntry = 0

;A PXE loader really needs nothing except this init:
use16
        org     7C00h
microkernelStart:
;zero all segment registers except CS
	xor	ax,ax
	mov	ds,ax
	mov	es,ax
	mov	fs,ax
	mov	gs,ax
	mov	ss,ax
