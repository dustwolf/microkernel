;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah, Shane Tyler
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

	;I don't know why you moved this, but this is what you want to do, instead of crashing... -- Kohlrak

	;actually this is the correct macro... I don't think I realized that there is some use to having it in end.asm at the time <.< -- DustWolf
	halt32

hcf:
	jmp	0:0
filesz = $-microkernelStart+cdromEntry ;note: this is before the pad
times 800h - (filesz mod 800h) db 0 ;2kb Alignment
microkernelEnd:
