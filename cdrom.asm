;; ---------------------------------------------------------------------------
;;
;;	Copyright 2008,2009 Jure Sah, Shane Tyler Yorks
;;
;;	This file is part of microKernel.
;;
;;	microKernel is free software: you can redistribute it and/or modify
;;	it under the terms of the GNU General Public License as published by
;;	the Free Software Foundation, either version 3 of the License, or
;;	(at your option) any later version.
;;
;;	Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

format binary

include "debugger16Macros.asm"  ;contains macros for 16bit debugging
include "eltorito.asm"		;makes the kernel loadable from cdrom
include "stack16.asm"		;16bit stack
include "VESAflip.asm"		;changes to graphic mode
	cli			;DISABLE INTERRUPTS
include "debugger32Macros.asm"	;this contains the text printing macros used for debugging
include "transition32.asm"	;goes into 32bit protected mode
                                ;and loads the GDT (GDT.asm)
include "stack32.asm"		;32bit stack
	sti
include "debugger32.asm"	;this contains the DBug_PrintNum and DBug_PrintString functions
	;initMouse ;volatile without mouse
include "pcspeaker.asm"		;initialize pc speaker support
include "graphics.asm"		;display graphics
include "fancy32.asm"		;the fancy stuff

include "end.asm"		;does a loop of eternal nothingness
