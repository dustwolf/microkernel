;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;Don't forget to change in GDT.asm ~ Kohlrak

mov ax, STACK_SELECTOR
mov ss, ax
mov esp, 1536
