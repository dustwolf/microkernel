;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;This file contains PS/2 mouse support

macro mouseACK {
;	WAITS FOR MOUSE TO ACK	this code prolly needs something smarter
;				like actually repeating the command until
;				it's accepted or something... dunno but
;				the way it is, if the mouse didn't get the
;				command, the code will hang.
@@:
	in	al,0x60
	cmp	al,0xFA
	jne	@b
}

macro initMouse {
	xor	eax,eax

;       ENABLE MOUSE and IRQ12
        Port64out       0x20    ;Read controller RAM, Command byte
        Port60in                ;get current setup
	mov	bl,al		;do math separately
        or      bl,00000011b    ;set bits 0 and 1
        and     bl,11001111b    ;clear bits 4,5

        Port64out       0x60    ;Write controller RAM, Command byte
        Port60out       bl      ;set new setup

;	RESET MOUSE	this may be a bit redundant and works better without
;	Port64out	0xD4	;to mouse
;	Port60out	0xFF	;reset mouse
;	mouseACK		;it's probable this ACK never comes

;	Port64out	0xD4	;to mouse
;	Port60out	0xF6	;Defaults
;	mouseACK

;	CONFIGURE MOUSE	this may be way reduntant and works better without
;	Port64out	0xD4	;to mouse
;	Port60out	0xF3	;Set Sample Rate
;	mouseACK
;	Port64out	0xD4
;	Port60out	10	;10 samples per second
;	mouseACK
	
;	Port64out	0xD4
;	Port60out	0xE8	;Set Resolution
;	mouseACK
;	Port64out	0xD4
;	Port60out	3	;8 counts / mm
;	mouseACK

;	Port64out	0xD4
;	Port60out	0xE6	;Set Scaling 1:1
;	mouseACK
	
;	ACTIVATE MOUSE
	Port64out	0xD4	;to mouse
	Port60out	0xF4	;Enable Data Reporting
	mouseACK
}
;	MOUSE CODE HERE	happens on IRQ12
;       COLLECT DATA
	cli
        xor     eax, eax
	mov	ecx, [mouseIndex]
        in      al, 0x60
	mov	[mouseBuffer+ecx],al
	inc	ecx
	mov	[mouseIndex],ecx
	cmp	ecx,3
	jne	@f			;if time to warp, execute below
	
	xor	ecx,ecx			;AMD <3
	mov	[mouseIndex],ecx


	mov	al,[mouseBuffer]
;       PRINT
	textCoordinates	0,12
        mov     esi, eax
        call    DBug_PrintNum
        mov     al,[mouseBuffer+1]
;       PRINT
	textCoordinates	0,13
        mov     esi, eax
        call    DBug_PrintNum
        mov     al,[mouseBuffer+2]
;       PRINT
	textCoordinates	0,14
        mov     esi, eax
        call    DBug_PrintNum
        mov     al,[mouseBuffer+3]
;       PRINT
	textCoordinates	0,15
        mov     esi, eax
        call    DBug_PrintNum


	jmp	@f
mouseIndex	dd	0	;which is the current buffer offset
mouseBuffer	rb	5	;3 bytes + 1 null + 1 justincase
@@:
	sti
