;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

; This code isn't currently used since the boot is too quick
; I'm leaving it in the sources for a while though

;This is supposed to be the fancy text printed out 
;durring the 16 bit mode time of the boot

;It's basically a copy of the helloworld.asm code

macro debug16GetPage {
        mov     ah,0Fh
        int     10h     ;get screen page
}

;debug16Type messageoffset,end_char
macro debug16Type message, end {
        mov     si,message
        mov     ah,0Eh  ;teletype output
@@:
        mov     al,[si]
        int     10h
        inc     si
        cmp     al,end
        jne     @b
}

	debug16GetPage
	debug16Type welcomeMessage,0

;       jump over own data to the end
        jmp     @f

;       DATA
welcomeMessage  db      'Booting microKernel...',13,10
		db	13,10
		db	'Written by:',13,10
		db	'DustWolf and Kohlrak',13,10
		db	'http://microkernel.ctrl-alt-del.si',13,10
		db	0

@@:
