;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------
;
; This file initializes the IDT

	;Initialize Programmable Interrupt Controller
	mov al, 00010001b
	out 0x20, al
	out 0xA0, al

	;Map the IRQs to ISRs
	mov al, 0x20
	out 0x21, al
	mov al, 0x28
	out 0xA1, al

	;Inform the master and slave pics of each other.
	mov al, 4
	out 0x21, al
	mov al, 2
	out 0xA1, al

	;Finally, finish by saying we're on an 80x86 and we want to use Automatic EOI
	mov al, 00000011b
	out 0x21, al
	out 0xA1, al

	mov al, 00000000b
	out 0x21, al
	mov al, 00000000b
	out 0xA1, al

	lidt	[IDTR]			; load IDT register
	
	jmp	endIDT

include "IDT.asm"
include	"IDTroutines.asm"
	
endIDT:
