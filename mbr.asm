;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008 Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

use16
	org	7C00h
microkernelStart:
	jmp	xEntry
	nop
	rb	25	;11 + 11 + 3zavsakslucaj

xEntry:
;       INIT
	xor	ax,ax
	mov	ds,ax	;set data segment to 0

include "stack16.asm"           ;16bit stack

;       MAIN
tryBoot:
	xor	ax,ax
	mov	es,ax		;segment = 0
	mov	ah,2		;read
	mov	al,20		;read 20 sectors (this should be replaced with something automatic)
	mov	cx,1		;sector=1 cylinder=0
	mov	dh,0		;head=0
	mov	dl,0		;drive=floppy A (80h=hdd)
	mov	bx,7C00h	;overwrite us with us
	int	13h	;load Boot into 
	jnc	boot

	mov	ah,0Fh
	int	10h	;Get screen page
	mov	si,bootErrorMsg
	mov	ah,0Eh	;teletype output
@@:
	mov	al,[si]
	int	10h
	inc	si
	cmp	al,10
	jne	@b

	jmp	tryBoot

;       DATA
bootErrorMsg	db	'unable to read disk.',13,10

	times	(7DFEh-$)	db ?
		db	55h,0AAh

boot:
