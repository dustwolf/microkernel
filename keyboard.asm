;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Shane Tyler Yorks, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------
;
; This file contains the keyboard ISR that is in IDTroutines.asm

;	READ
	cli
        xor	eax, eax
        in	al, 0x60

;	PRINT
	textCoordinates	0,9
	mov	esi, eax
	call	DBug_PrintNum

	sti
;	EOI
        in	al, 0x61
        out	0x61, al 	;The keyboard's personal EOI. If manual EOI 
				;enabled by PIC, you must EOI to the pic, too.
				;But for now, AEOI is enabled for the PIC.

