;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------
;
; I basically copied this out of the VESA code, with the purpose in mind to
; keep it as compatible as possible... to be able to use the same code in
; both cases - less confusion and if we're lucky even some degree of
; interoperability. -- DustWolf

TEXT_WIDTH      equ	80d	;this is a guess but if this were DOS it'd
TEXT_HEIGHT	equ	25d	;be correct
SCREEN_OFFSET	equ	2d
SCREEN_TOP	equ	SCREEN_OFFSET
SCREEN_BOTTOM	equ	(TEXT_HEIGHT-SCREEN_OFFSET)
SCREEN_LEFT	equ	SCREEN_OFFSET
SCREEN_RIGHT	equ	(TEXT_WIDTH-SCREEN_OFFSET)

;this is here for now probably best kept here since this may change if
;somebody picks a weird resolution

macro coordinates register,X,Y {
	mov	register,((X)+(Y)*TEXT_WIDTH)*2
}

macro addCoordinates register,X,Y {
	add	register,((X)+(Y)*TEXT_WIDTH)*2
}

macro subCoordinates register,X,Y {
        sub     register,((X)+(Y)*TEXT_WIDTH)*2
}
