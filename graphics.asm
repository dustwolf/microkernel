;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Jure Sah, Shane Tyler Yorks
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;This file displays the graphics of the kernel, 32bit code

; split this into graphics.asm and fancy32.asm
; partly because that's what it is and
; partly because this needs VESA and the other thing doesn't
;		-- DustWolf

include	"colorscheme.asm"	;this contains the color constants to be used here

;	SELECT VESA GRAPHICS SEGMENT
	mov 	ax, VESA_SELECTOR
	mov 	es, ax

include "background.asm"	;draw the background
