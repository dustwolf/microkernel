;; ---------------------------------------------------------------------------
;;
;;      Copyright 2008,2009 Shane Tyler Yorks, Klemen Forstnerič, Jure Sah
;;
;;      This file is part of microKernel.
;;
;;      microKernel is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; ---------------------------------------------------------------------------

;This file goes to 32bit

include "A20.asm"		;this switches on A20 to allow wider memory access
	

	lgdt	[GDTR]			; load GDT register

	mov	eax,cr0 		; switch to protected mode
	or	al,1
	mov	cr0,eax

	jmp	CODE_SELECTOR:pm_start

include	"GDT.asm"

	USE32

pm_start:

	;note: only ES *NEEDS* to be set...
	mov	ax, DATA_SELECTOR	; load 4 GB data descriptor
	mov	ds,ax			; to all data segment registers
	jmp	CODE_SELECTOR:ia32_start

ia32_start:

include "loadIDT.asm"
